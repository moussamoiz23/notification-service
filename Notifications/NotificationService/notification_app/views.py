from rest_framework import viewsets, response
from django.db.models import Count, Q
from drf_yasg.utils import swagger_auto_schema
from .serializers import *
from .tasks import send_mailing
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler('notification_app.log')
file_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(file_handler)


class ClientViewSet(viewsets.ModelViewSet):
    """
    Представление клиента
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    @swagger_auto_schema(
        operation_description='List all clients',
        responses={200: ClientSerializer(many=True)}
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Retrieve a client',
        responses={200: ClientSerializer}
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Create a new client',
        request_body=ClientSerializer,
        responses={201: ClientSerializer}
    )
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        logger.info(f'Created client {response.data["id"]}')
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Update a client',
        request_body=ClientSerializer,
        responses={200: ClientSerializer}
    )
    def update(self, request, *args, **kwargs):
        logger.info(f'Updated client {kwargs["pk"]}')
        return super().update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Partially update a client',
        request_body=ClientSerializer,
        responses={200: ClientSerializer}
    )
    def partial_update(self, request, *args, **kwargs):
        logger.info(f'Partially updated client {kwargs["pk"]}')
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Delete a client',
        responses={204: ''}
    )
    def destroy(self, request, *args, **kwargs):
        logger.info(f'Deleted client {kwargs["pk"]}')
        return super().destroy(request,*args,**kwargs)


class MailingViewSet(viewsets.ModelViewSet):
    """
    Представление рассылки
    """
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        super().perform_create(serializer)
        send_mailing.delay(serializer.instance.id)

    @swagger_auto_schema(
        operation_description='List all mailings',
        responses={200: MailingSerializer(many=True)}
    )
    def list(self,request,*args,**kwargs):
        return super().list(request,*args,**kwargs)

    @swagger_auto_schema(
        operation_description='Retrieve a mailing',
        responses={200: MailingSerializer}
    )
    def retrieve(self,request,*args,**kwargs):
        return super().retrieve(request,*args,**kwargs)

    @swagger_auto_schema(
        operation_description='Create a new mailing',
        request_body=MailingSerializer,
        responses={201: MailingSerializer}
    )
    def create(self,request,*args,**kwargs):
        response = super().create(request, *args, **kwargs)
        logger.info(f'Created mailing {response.data["id"]}')
        return super().create(request,*args,**kwargs)

    @swagger_auto_schema(
        operation_description='Update a mailing',
        request_body=MailingSerializer,
        responses={200: MailingSerializer}
    )
    def update(self, request, *args, **kwargs):
        logger.info(f'Updated mailing {kwargs["pk"]}')
        return super().update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Partially update a mailing',
        request_body=MailingSerializer,
        responses={200: MailingSerializer}
    )
    def partial_update(self, request, *args, **kwargs):
        logger.info(f'Partially updated mailing {kwargs["pk"]}')
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description='Delete a mailing',
        responses={204: ''}
    )
    def destroy(self, request, *args, **kwargs):
        logger.info(f'Deleted mailing {kwargs["pk"]}')
        return super().destroy(request,*args,**kwargs)


class MessageViewSet(viewsets.ModelViewSet):
    """ Представление сообщения"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    @swagger_auto_schema(
      operation_description='List all messages',
      responses={200: MessageSerializer(many=True)}
    )
    def list(self,request,*args,**kwargs):
      return super().list(request,*args,**kwargs)

    @swagger_auto_schema(
        operation_description='Retrieve a message',
        responses={200: MessageSerializer}
    )
    def retrieve(self,request,*args,**kwargs):
        return super().retrieve(request,*args,**kwargs)


class ClientStatsViewSet(viewsets.ViewSet):
    """ Представление статистики клиента """

    @swagger_auto_schema(
        operation_description='List statistics for all clients',
        responses={200: 'List of statistics for all clients'}
    )
    def list(self,request):
        total_sent_count = Message.objects.filter(status='sent').count()
        total_not_sent_count = Message.objects.filter(status='not_sent').count()
        total_error_count = Message.objects.filter(status='error').count()

        total_statistics = {
          'total_sent_count': total_sent_count,
          'total_not_sent_count': total_not_sent_count,
          'total_error_count': total_error_count,
        }

        client_stats = Client.objects.annotate(
          sent_count=Count('message', filter=Q(message__status='sent')),
          not_sent_count=Count('message', filter=Q(message__status='not_sent')),
          error_count=Count('message', filter=Q(message__status='error')),
        )

        client_statistics = [
          {
              'client_id': client.id,
              'sent_count': client.sent_count,
              'not_sent_count': client.not_sent_count,
              'error_count': client.error_count,
          }
          for client in client_stats
        ]

        data = {
          'total_statistics': total_statistics,
          'client_statistics': client_statistics,
        }

        return response.Response(data)

