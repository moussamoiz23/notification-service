from django.contrib import admin
from .models import Client, Mailing, Message
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler('notification_app.log')
file_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(file_handler)


class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'operator_code', 'phone_number', 'tag', 'timezone_offset']
    search_fields = ["phone_number"]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if change:
            logger.info(f'Updated client {obj.id} via admin panel')
        else:
            logger.info(f'Created client {obj.id} via admin panel')

    def delete_model(self, request, obj):
        logger.info(f'Deleted client {obj.id} via admin panel')
        super().delete_model(request, obj)


class MailingAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'operator_code_filter', 'tag_filter', 'start_time', 'end_time', 'time_interval_start', 'time_interval_end'
    ]
    search_fields = ["id"]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if change:
            logger.info(f'Updated mailing {obj.id} via admin panel')
        else:
            logger.info(f'Created mailing {obj.id} via admin panel')

    def delete_model(self, request, obj):
        logger.info(f'Deleted mailing {obj.id} via admin panel')
        super().delete_model(request, obj)


class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'created_at', 'status']
    fields = ('status', 'mailing', 'client')
    search_fields = ["status"]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if change:
            logger.info(f'Updated message {obj.id} via admin panel')
        else:
            logger.info(f'Created message {obj.id} via admin panel')

    def delete_model(self, request, obj):
        logger.info(f'Deleted message {obj.id} via admin panel')
        super().delete_model(request, obj)


admin.site.register(Client, ClientAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)


