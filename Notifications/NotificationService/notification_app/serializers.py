import re

from rest_framework import serializers
from .models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def validate_phone_number(self, value):
        if not re.match(r'^7\d{10}$', value):
            raise serializers.ValidationError('Номер телефона должен быть в формате 7XXXXXXXXXX')
        return value


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class ClientStatsSerializer(serializers.Serializer):
    client_id = serializers.IntegerField()
    sent_count = serializers.IntegerField()
    not_sent_count = serializers.IntegerField()
    error_count = serializers.IntegerField()

