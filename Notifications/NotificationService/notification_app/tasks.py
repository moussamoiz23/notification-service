import os
import uuid
from datetime import timedelta

from celery import shared_task
import requests as http_requests
from django.utils import timezone
from django.core.mail import send_mail
from django.db.models import Count, Q
from dotenv import load_dotenv
import logging

from .models import Mailing, Client, Message

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

file_handler = logging.FileHandler('notification_app.log')
file_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(file_handler)

load_dotenv()


@shared_task
def send_mailing(mailing_id):
    logger.info(f'Starting mailing {mailing_id}')
    mailing = Mailing.objects.get(id=mailing_id)

    if not (mailing.start_time <= timezone.now() <= mailing.end_time):
        logger.info(f'Mailing {mailing_id} is not in the time interval')
        return

    clients = Client.objects.all()

    if mailing.operator_code_filter:
        clients = clients.filter(operator_code=mailing.operator_code_filter)

    if mailing.tag_filter:
        clients = clients.filter(tag=mailing.tag_filter)

    for client in clients:
        logger.info(f'Processing client id: {client.id}')
        if mailing.time_interval_start and mailing.time_interval_end:
            client_local_time = (timezone.now() + timedelta(hours=client.timezone_offset)).time()
            if mailing.time_interval_start <= client_local_time <= mailing.time_interval_end:
                logger.info(f'Client {client.id} is in the time interval')
                continue

        logger.info(f'Sending message to client id {client.id}')
        msg_id = uuid.uuid4().int
        token = os.getenv('ACCESS_TOKEN')
        url = f'https://probe.fbrq.cloud/v1/send/{msg_id}'
        headers = {
            'Authorization': f'Bearer {token}',
        }
        data = {
            'id': msg_id,
            'phone': client.phone_number,
            'text': mailing.message_text,
        }
        try:
            response = http_requests.post(url, headers=headers, json=data)
            response.raise_for_status()
            status = 'sent'
            logger.info(f'Message to client {client.id} sent successfully')

        except Exception as exc:
            logger.exception(f'Error sending message to client {client.id}: {exc}')
            logger.error(f'Response content: {response.content}')
            status = 'error'

        logger.info(f'Creating message for client {client.id}')
        message = Message.objects.create(
            mailing=mailing,
            client=client,
            status=status,
        )
    mailing.is_sent = True
    mailing.save()
    logger.info(f'Finished mailing {mailing_id}')


@shared_task
def check_mailings():
    now = timezone.now()
    mailings = Mailing.objects.filter(start_time__lte=now, end_time__gte=now, is_sent=False)
    for mailing in mailings:
        send_mailing.delay(mailing.id)


@shared_task
def send_daily_statistics():
    """ форматирование общей статистики и статистики по клиентам для отправки по электронной почте """

    total_sent_count = Message.objects.filter(status='sent').count()
    total_not_sent_count = Message.objects.filter(status='not_sent').count()
    total_error_count = Message.objects.filter(status='error').count()

    statistics = f"""
        DAILY STATISTICS:
        Total sent: {total_sent_count}
        Total not sent: {total_not_sent_count}
        Total errors: {total_error_count}
    """

    client_stats = Client.objects.annotate(
        sent_count=Count('message', filter=Q(message__status='sent')),
        not_sent_count=Count('message', filter=Q(message__status='not_sent')),
        error_count=Count('message', filter=Q(message__status='error')),
    )

    client_statistics = '\n'.join([
        f"Client {client.id}: sent={client.sent_count}, not sent={client.not_sent_count}, errors={client.error_count}"
        for client in client_stats
    ])

    from_email = os.getenv('EMAIL_HOST_USER')
    to_email = [os.getenv('DAILY_STATISTICS_EMAIL')]

    send_mail(
        'Daily Statistics',
        statistics + '\n\n' + client_statistics,
        from_email,
        to_email,
        fail_silently=False,
    )

