from rest_framework import serializers
from django.test import TestCase
from django.utils import timezone
from datetime import timedelta
from notification_app.models import Client, Mailing, Message
from notification_app.serializers import ClientSerializer, MailingSerializer, MessageSerializer, ClientStatsSerializer

class ClientSerializerTest(TestCase):
    def test_serializer_fields(self):
        client = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset=3)
        serializer = ClientSerializer(client)
        data = serializer.data
        self.assertEqual(set(data.keys()), set(['id', 'phone_number', 'operator_code', 'tag', 'timezone_offset']))
        self.assertEqual(data['phone_number'], '71234567890')
        self.assertEqual(data['operator_code'], 'MTS')
        self.assertEqual(data['tag'], 'VIP')
        self.assertEqual(data['timezone_offset'], 3)

    def test_phone_number_validation(self):
        serializer = ClientSerializer(data={'phone_number': '1234567890', 'operator_code': 'MTS', 'tag': 'VIP', 'timezone_offset': 3})
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['phone_number'], ['Номер телефона должен быть в формате 7XXXXXXXXXX'])


class MailingSerializerTest(TestCase):
    def test_serializer_fields(self):
        start_time = timezone.now()
        end_time = start_time + timedelta(days=1)
        mailing = Mailing.objects.create(start_time=start_time, end_time=end_time, message_text='Test message')
        serializer = MailingSerializer(mailing)
        data = serializer.data
        self.assertEqual(set(data.keys()), set(['id', 'start_time', 'end_time', 'message_text', 'operator_code_filter', 'tag_filter', 'time_interval_start', 'time_interval_end', 'is_sent']))
        self.assertEqual(data['start_time'], start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        self.assertEqual(data['end_time'], end_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'))
        self.assertEqual(data['message_text'], 'Test message')


class MessageSerializerTest(TestCase):
    def test_serializer_fields(self):
        client = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset=3)
        mailing = Mailing.objects.create(start_time=timezone.now(), end_time=timezone.now() + timedelta(days=1), message_text='Test message')
        message = Message.objects.create(status='sent', mailing=mailing, client=client)
        serializer = MessageSerializer(message)
        data = serializer.data
        self.assertEqual(set(data.keys()), set(['id', 'created_at', 'status', 'mailing', 'client']))
        self.assertEqual(data['status'], 'sent')
        self.assertEqual(data['mailing'], mailing.id)
        self.assertEqual(data['client'], client.id)


class ClientStatsSerializerTest(TestCase):
    def test_serializer_fields(self):
        data = {
            'client_id': 1,
            'sent_count': 10,
            'not_sent_count': 5,
            'error_count': 2,
        }
        serializer = ClientStatsSerializer(data=data)
        self.assertTrue(serializer.is_valid())
