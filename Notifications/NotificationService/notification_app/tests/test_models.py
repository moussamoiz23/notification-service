from django.test import TestCase
from django.utils import timezone
from notification_app.models import Client, Mailing, Message
import datetime


class ClientModelTest(TestCase):
    def test_client_fields(self):
        client = Client.objects.create(
            phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset=3
        )
        self.assertEqual(client.phone_number, '71234567890')
        self.assertEqual(client.operator_code, 'MTS')
        self.assertEqual(client.tag, 'VIP')
        self.assertEqual(client.timezone_offset, 3)


class MailingModelTest(TestCase):
    def test_mailing_fields(self):
        start_time = timezone.now()
        end_time = start_time + datetime.timedelta(days=1)
        mailing = Mailing.objects.create(
            start_time=start_time, end_time=end_time, message_text='Test message', operator_code_filter='MTS', tag_filter='VIP'
        )
        self.assertEqual(mailing.start_time, start_time)
        self.assertEqual(mailing.end_time, end_time)
        self.assertEqual(mailing.message_text, 'Test message')
        self.assertEqual(mailing.operator_code_filter, 'MTS')
        self.assertEqual(mailing.tag_filter, 'VIP')
        self.assertFalse(mailing.is_sent)


class MessageModelTest(TestCase):
    def setUp(self):
        self.client = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset=3)
        self.mailing = Mailing.objects.create(start_time=timezone.now(), end_time=timezone.now() + datetime.timedelta(days=1), message_text='Test message')

    def test_message_fields(self):
        message = Message.objects.create(status='sent', mailing=self.mailing, client=self.client)
        self.assertEqual(message.status, 'sent')
        self.assertEqual(message.mailing, self.mailing)
        self.assertEqual(message.client, self.client)
        self.assertIsInstance(message.created_at, datetime.datetime)

    def test_message_foreign_keys(self):
        message = Message.objects.create(mailing=self.mailing, client=self.client)
        self.assertEqual(message.mailing, self.mailing)
        self.assertEqual(message.client, self.client)

    def test_message_foreign_key_deletion(self):
        message = Message.objects.create(mailing=self.mailing, client=self.client)
        self.mailing.delete()
        with self.assertRaises(Message.DoesNotExist):
            message.refresh_from_db()

        self.mailing = Mailing.objects.create(start_time=timezone.now(),
                                              end_time=timezone.now() + datetime.timedelta(days=1),
                                              message_text='Test message')
        message = Message.objects.create(mailing=self.mailing, client=self.client)
        self.client.delete()
        with self.assertRaises(Message.DoesNotExist):
            message.refresh_from_db()


