from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.test import APIClient
from notification_app.models import Client, Mailing, Message
from django.utils import timezone
import datetime


class ClientViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.client1 = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP',
                                             timezone_offset=3)

    def test_list(self):
        url = reverse('clients-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['phone_number'], '71234567890')
        self.assertEqual(data[0]['operator_code'], 'MTS')
        self.assertEqual(data[0]['tag'], 'VIP')
        self.assertEqual(data[0]['timezone_offset'], 3)

    def test_create(self):
        url = reverse('clients-list')
        data = {'phone_number': '79876543210', 'operator_code': 'Beeline', 'tag': 'Regular', 'timezone_offset': 3}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.json()
        self.assertEqual(data['phone_number'], '79876543210')
        self.assertEqual(data['operator_code'], 'Beeline')
        self.assertEqual(data['tag'], 'Regular')
        self.assertEqual(data['timezone_offset'], 3)

    def test_retrieve(self):
        url = reverse('clients-detail', args=[self.client1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['phone_number'], '71234567890')
        self.assertEqual(data['operator_code'], 'MTS')
        self.assertEqual(data['tag'], 'VIP')
        self.assertEqual(data['timezone_offset'], 3)

    def test_update(self):
        url = reverse('clients-detail', args=[self.client1.id])
        data = {'phone_number': '79876543210', 'operator_code': 'Beeline', 'tag': 'Regular', 'timezone_offset': 3}
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['phone_number'], '79876543210')
        self.assertEqual(data['operator_code'], 'Beeline')
        self.assertEqual(data['tag'], 'Regular')
        self.assertEqual(data['timezone_offset'], 3)

    def test_destroy(self):
        url = reverse('clients-detail', args=[self.client1.id])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class MailingViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        start_time = timezone.now()
        end_time = start_time + datetime.timedelta(days=1)
        self.mailing1 = Mailing.objects.create(start_time=start_time, end_time=end_time, message_text='Test message')

    def test_list(self):
        url = reverse('mailings-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(len(data), 1)

    def test_create(self):
        url = reverse('mailings-list')
        start_time = timezone.now()
        end_time = start_time + datetime.timedelta(days=1)
        data = {
            'start_time': start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            'end_time': end_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            'message_text': 'Test message',
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_retrieve(self):
        url = reverse('mailings-detail', args=[self.mailing1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['id'], self.mailing1.id)

    def test_update(self):
        url = reverse('mailings-detail', args=[self.mailing1.id])
        start_time = timezone.now()
        end_time = start_time + datetime.timedelta(days=2)
        data = {
            'start_time': start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            'end_time': end_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            'message_text': 'Updated message',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['message_text'], 'Updated message')

    def test_partial_update(self):
        url = reverse('mailings-detail', args=[self.mailing1.id])
        data = {'message_text': 'Updated message'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['message_text'], 'Updated message')

    def test_destroy(self):
        url = reverse('mailings-detail', args=[self.mailing1.id])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class MessageViewSetTest(APITestCase):
    def setUp(self):
        self.client_obj = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset='3')
        self.mailing = Mailing.objects.create(start_time=timezone.now(), end_time=timezone.now() + datetime.timedelta(days=1), message_text='Test message')

    def test_create_message(self):
        url = reverse('messages-list')
        data = {'status': 'sent', 'mailing': self.mailing.id, 'client': self.client_obj.id}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.count(), 1)
        self.assertEqual(Message.objects.get().status, 'sent')


class ClientStatsViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.client1 = Client.objects.create(phone_number='71234567890', operator_code='MTS', tag='VIP', timezone_offset=3)
        self.client2 = Client.objects.create(phone_number='79876543210', operator_code='Beeline', tag='Regular', timezone_offset=3)
        self.mailing = Mailing.objects.create(start_time=timezone.now(), end_time=timezone.now() + datetime.timedelta(days=1), message_text='Test message')
        Message.objects.create(status='sent', mailing=self.mailing, client=self.client1)
        Message.objects.create(status='not_sent', mailing=self.mailing, client=self.client1)
        Message.objects.create(status='error', mailing=self.mailing, client=self.client2)

    def test_list(self):
        url = reverse('client-stats-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data['total_statistics']['total_sent_count'], 1)
        self.assertEqual(data['total_statistics']['total_not_sent_count'], 1)
        self.assertEqual(data['total_statistics']['total_error_count'], 1)
        self.assertEqual(len(data['client_statistics']), 2)
