from django.db import models
from django.utils import timezone


class Client(models.Model):
    """
    Модель клиента
    """
    phone_number = models.CharField(max_length=11, verbose_name='Номер телефона')
    operator_code = models.CharField(max_length=50, verbose_name='Код оператора')
    tag = models.CharField(max_length=50, verbose_name='Тег')
    timezone_offset = models.IntegerField(verbose_name='Смещение часового пояса')

    def __str__(self) -> str:
        return f'Клиент {self.phone_number}'


class Mailing(models.Model):
    """
    Модель рассылки
    """
    start_time = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    end_time = models.DateTimeField(verbose_name='Дата и время окончания рассылки')
    message_text = models.TextField(verbose_name='Текст сообщения')
    operator_code_filter = models.CharField(max_length=50, verbose_name='Фильтр по коду оператора', blank=True)
    tag_filter = models.CharField(max_length=50, verbose_name='Фильтр по тегу', blank=True)
    time_interval_start = models.TimeField(null=True, blank=True, verbose_name='Начало временного интервала')
    time_interval_end = models.TimeField(null=True, blank=True, verbose_name='Конец временного интервала')
    is_sent = models.BooleanField(default=False, verbose_name='Отправлено')

    def __str__(self) -> str:
        return f'Рассылка {self.id}'


class Message(models.Model):
    """
    Модель сообщения
    """
    STATUS_CHOICES = (
        ('sent', 'Отправлено'),
        ('not_sent', 'Не отправлено'),
        ('error', 'Ошибка отправки'),
    )

    created_at = models.DateTimeField(default=timezone.now, verbose_name='Дата и время создания')
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='not_sent', verbose_name='Статус отправки')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='Рассылка')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Клиент')

    def __str__(self) -> str:
        return f'Сообщение {self.id}'

