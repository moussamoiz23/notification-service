# import os
# import requests
# from django.test import TestCase, Client
# from django.urls import reverse
# from django.contrib.auth import get_user_model
# from .views import index, login, callback, logout
#
# User = get_user_model()
#
#
# class Auth0ViewsTests(TestCase):
#     def setUp(self):
#         self.client = Client()
#
#     def test_index_view(self):
#         response = self.client.get(reverse("index"))
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, "index.html")
#
#     def test_login_view(self):
#         response = self.client.get(reverse("login"))
#         self.assertEqual(response.status_code, 302)
#         self.assertIn("Location", response)
#         redirect_url = response["Location"]
#         self.assertTrue(redirect_url.startswith(f"https://{os.environ.get('AUTH0_DOMAIN')}/authorize"))
#
#     def test_callback_view(self):
#         # Send request to login view to get redirected to Auth0 login
#         response = self.client.get(reverse("login"))
#         self.assertEqual(response.status_code, 302)
#         self.assertIn("Location", response)
#         redirect_url = response["Location"]
#
#         # Now we have the 'code', proceed with the callback test
#         mock_token = {
#             "access_token": "eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIiwiaXNzIjoiaHR0cHM6Ly9kZXYtbHdmMWh5dnpuNHlrYndyMS51cy5hdXRoMC5jb20vIn0..jSv6J4OEj3w8LsKx.AcIV2D1-85jNBqqtOghnhXznsT3gFkTOEfrY8eRKa_K9dsThBllB9lqX7AU2sqLubk6oCbrNmgSgAnUImU-3XoM6hVNXFYZ8O5xj4QBCJBE6P9FFKT-9OMH5WtFuKNlLkOyTtbXCnZ6U1YCHh4Jli7sbPPzOE7HncLvlcm682UzMq2vtqh61TE4CoSTX7rY-a0DLd_pdC-FGSF_vppHJ7q0qY8K5dzFKeeW4zHQBpykmwOQO35eTVRmAYoaMFy3YC-BQyKwNYVkc-PzpweRC64CbanslRJYojjCTaKtG5Fl2wyRglcUaWmQDOYMFxps7Qpz290EHl0z33FeZ8dXqTg8M.wLy4LNyZsiMhxYUtP7URpQ",
#             "id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjFyRHgtM0wzU2JLM0hnMk9PeHMxbCJ9.eyJuaWNrbmFtZSI6Imt1aWF0ZS5tIiwibmFtZSI6Imt1aWF0ZS5tQG1haWwucnUiLCJwaWN0dXJlIjoiaHR0cHM6Ly9zLmdyYXZhdGFyLmNvbS9hdmF0YXIvN2ViMTJkYmM4YTI2NGQxNGZjZTQ4MWVkMmIxY2FhZTE_cz00ODAmcj1wZyZkPWh0dHBzJTNBJTJGJTJGY2RuLmF1dGgwLmNvbSUyRmF2YXRhcnMlMkZrdS5wbmciLCJ1cGRhdGVkX2F0IjoiMjAyMy0wOC0wNVQxNjozNzo0NS4yNTBaIiwiZW1haWwiOiJrdWlhdGUubUBtYWlsLnJ1IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vZGV2LWx3ZjFaeXZ6bjR5a2J3cjEudXMuYXV0aDAuY29tLyIsImF1ZCI6IkhycVR4VHh5dmFlRlhWdThqWjhCOGJXY0V4djBDNUdYIiwiaWF0IjoxNjkxMjUzNDY2LCJleHAiOjE2OTEyODk0NjYsInN1YiI6ImF1dGgwfDY0YzkxOTMzYWRmMGE5NGMzM2QxZGRmMiIsInNpZCI6IkpEYnFjTFgxb3pVeDR0ZVpIdW9EcnFjUGIzQlNod3dIIiwibm9uY2UiOiJXNkhZWWRDWVdnUzVjRHZ1WDBhaCJ9.cfO2sGn5oNlSjEsR5KSQgoVWBKqST_Bdg0szLAs0WtHLgOM0xxfXDHv7q6g1Tbehb69mNEaQALHVWypsn-v73HWlnkKyDwZ_NPRu9wpXghCf47F5_foC8U9_w2A-L-aMTul5SUD5Ya0IZxNwkfF9m2rQ6CDxqSXoB_G30k2l-2KbK7anOFXvEPA0yNsFXZ9FLsYguizKZ6V0FBimyNc5L6-dr55GwmjryzkHeiVzlUnS1KWjvh07jHQ2E2IJTnDmJeAyrTpRUN8rFBRyDuYgAL2WU00_JH_E-KsudRCGSH6BV3_SAUpHJQtSiYGk7BT5iLKtBxUNsLhh63lsl1FJPA",
#             "code": "test_code",
#             "state": "test_state"
#         }
#
#         # Set the session data for the client
#         session = self.client.session
#         session.update(mock_token)
#         session.save()
#
#         user = User.objects.create_user(username="testuser", password="testpassword")
#         self.client.force_login(user)
#
#         # Make the callback request
#         response = self.client.get(reverse("callback"), data=mock_token)
#         self.assertEqual(response.status_code, 302)
#         self.assertIn("Location", response)
#         redirect_url = response["Location"]
#         self.assertTrue(redirect_url.endswith(reverse("index")))
#
#     def test_logout_view(self):
#         response = self.client.get(reverse("logout"))
#         self.assertEqual(response.status_code, 302)
#         self.assertIn("Location", response)
#         redirect_url = response["Location"]
#         self.assertTrue(redirect_url.startswith(f"https://{os.environ.get('AUTH0_DOMAIN')}/v2/logout"))
#
import json
from django.test import TestCase, Client
from django.urls import reverse
from unittest.mock import patch, MagicMock

from .views import index, login, callback, logout

class OAuthTestCase(TestCase):
    def setUp(self):
        # создаем клиент для отправки запросов
        self.client = Client()

    def test_index_view_with_user_data(self):
        # Тестирование представления index с пользователями, которые авторизованы
        user_data = {
            "access_token": "mocked_access_token",
            "id_token": "mocked_id_token",
            "userinfo": {
                "name": "kuiate.m@mail.ru",
                "email": "kuiate.m@mail.ru",
                "email_verified": True,
            },
        }
        with self.settings(AUTH0_CLIENT_ID="mocked_client_id"):
            user = self.client.force_login(user_data)
            response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "kuiate.m@mail.ru")  # Проверяем, что имя пользователя отображается на странице

    @patch("authlib.integrations.django_client.OAuth")
    def test_login_view(self, mock_oauth):
        # Мокируем OAuth и его методы
        mock_oauth.return_value = MagicMock()
        mock_oauth.authorize_redirect.return_value = MagicMock(status_code=302)
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 302)
        mock_oauth.authorize_redirect.assert_called_once()

    @patch("authlib.integrations.django_client.OAuth")
    def test_callback_view(self, mock_oauth):
        # Мокируем OAuth и его методы
        mock_oauth.return_value = MagicMock()
        mock_oauth.authorize_access_token.return_value = {"access_token": "mocked_token"}
        csrf_token = "mocked_csrf_token"
        response = self.client.get(reverse("callback"), HTTP_X_CSRFTOKEN=csrf_token)
        self.assertEqual(response.status_code, 302)
        self.assertIn("user", self.client.session)

    def test_logout_view(self):
        # Тестирование представления logout
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)
        self.assertNotIn("user", self.client.session)
