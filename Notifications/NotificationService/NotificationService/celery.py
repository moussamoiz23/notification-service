from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NotificationService.settings')

app = Celery('NotificationService')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'check-mailings': {
        'task': 'notification_app.tasks.check_mailings',
        'schedule': 60.0,
    },
    'send-daily-statistics': {
        'task': 'notification_app.tasks.send_daily_statistics',
        'schedule': crontab(hour=0, minute=0),
    },
}
